import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void test1() {
        long expected = Main.getFirstMaxNumeral(0l);
        long actual = 0l;
        assertEquals(expected, actual);
    }

    @Test
    void test2() {
        long expected = Main.getFirstMaxNumeral(123);
        long actual = 3;
        assertEquals(expected, actual);
    }

    @Test
    void test3() {
        long expected = Main.getFirstMaxNumeral(320);
        long actual = 3;
        assertEquals(expected, actual);
    }

    @Test
    void test4() {
        long expected = Main.getFirstMaxNumeral(8);
        long actual = 8;
        assertEquals(expected, actual);
    }

    @Test
    void test5() {
        long expected = Main.getFirstMaxNumeral(98257);
        long actual = 9;
        assertEquals(expected, actual);
    }

    @Test
    void test6() {
        boolean expected = Main.validation(-25252);
        boolean actual = true;
        assertEquals(expected, actual);
    }

    @Test
    void test7() {
        boolean expected = Main.validation(99999999 * 999999999);
        boolean actual = false;
        assertEquals(expected, actual);
    }

    @Test
    void test8() {
        boolean expected = Main.validation(0);
        boolean actual = true;
        assertEquals(expected, actual);
    }

    @Test
    void test9() {
        boolean expected = Main.validation(1);
        boolean actual = true;
        assertEquals(expected, actual);
    }

    @Test
    void test10() {
        boolean expected = Main.validation(-1);
        boolean actual = true;
        assertEquals(expected, actual);
    }

}