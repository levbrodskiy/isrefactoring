import java.util.Scanner;

/**
 * Программа принимает на вход целое число, модуль которого не больше 10 в 8 степени.
 * Если введены неверные данные, выводит в консоль предупреждение об ошибке.
 * Если введены подходящие данные, выводит сообщение с ответом.
 * Ответом является первая наибольшая цифра из введённого числа.
 */
public class Main implements ConnectionWithUser {
    /**
     * Запуск программы
     */
    public static void main(String[] args) {
        long inputNumb;
        try {
            System.out.println(ENTER_NUMB);
            inputNumb = getInputData();
            System.out.println(ANSWER + getFirstMaxNumeral(inputNumb));
        }catch (Exception e){
            System.out.println(INVALID_DATA);
            return;
        }
    }

    /**
     * Возвращает цисло, введённое в консоль.
     * Если число не удволетворяет условию программы, выбрасывает исключение.
     * @return - цисло, введённое в консоль
     * @throws Exception - число не удволетворяет условию программы
     */
    public static long getInputData() throws Exception {
        long inputNumb = 0L;
        try (Scanner scanner = new Scanner(System.in)) {
            inputNumb = scanner.nextLong();
            inputNumb = Math.abs(inputNumb);
            if (!validation(inputNumb)){
                throw new Exception();
            }
        }
        return inputNumb;
    }

    /**
     * Возвращает true, если модуль входящего числа меньше 10 в 8 степени.
     * Возвращает false, если модуль входящего числа больше или равен 10 в 8 степени.
     * @param inputNumb - число для проверки
     * @return - результат валидации
     */
    public static boolean validation(long inputNumb){
        return Math.abs(inputNumb) < Math.pow(10,8);
    }

    /**
     * Возвращает первую максимальную цифру данного числа
     * @param numb - целочисленное число
     * @return - первую максимальную цифру данного числа
     */
    public static long getFirstMaxNumeral(long numb){
        long maxNumb = 0;
        while (numb > 10){
            long checkNumb = (numb % 10);
            numb = (numb - maxNumb) / 10;
            if(checkNumb > maxNumb){
                maxNumb = checkNumb;
            }
        }
        if (numb > maxNumb){
            maxNumb = numb;
        }
        return maxNumb;
    }
}
